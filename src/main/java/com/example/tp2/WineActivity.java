package com.example.tp2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class WineActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);
        String Titre, Reg, Loc, Clim, Surf;
        final AlertDialog.Builder builder = new AlertDialog.Builder(WineActivity.this);
        builder.setTitle("Sauvegarde impossible");
        builder.setMessage("Le nom du vin doit être non vide.");
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            System.out.println("test");
            final Wine wine = extras.getParcelable("wine");
            Titre = wine.getTitle();
            Reg = wine.getRegion();
            Loc = wine.getLocalization();
            Clim = wine.getClimate();
            Surf = wine.getPlantedArea();

            final EditText titre = findViewById(R.id.wineName);
            titre.setText(Titre);
            final EditText reg = findViewById(R.id.editWineRegion);
            reg.setText(Reg);
            final EditText loc = findViewById(R.id.editLoc);
            loc.setText(Loc);
            final EditText clim = findViewById(R.id.editClimate);
            clim.setText(Clim);
            final EditText surf = findViewById(R.id.editPlantedArea);
            surf.setText(Surf);

            final Button button = findViewById(R.id.button);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (titre.getText().toString().matches("")) {
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }
                    else {
                        wine.setTitle(titre.getText().toString());
                        wine.setRegion(reg.getText().toString());
                        wine.setLocalization(loc.getText().toString());
                        wine.setClimate(clim.getText().toString());
                        wine.setPlantedArea(surf.getText().toString());
                        Intent intent = new Intent(WineActivity.this, MainActivity.class);
                        intent.putExtra("save", wine);
                        finish();
                        startActivity(intent);
                    }
                }
            });
        }
    }
}
